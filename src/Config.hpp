#pragma once

namespace cppkit
{
    using Integer = int;  // 整数型数据
    using Real = double;  // 实数型数值
}  // namespace cppkit
