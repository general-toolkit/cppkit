#pragma once

#include <fstream>
#include <iostream>
#include <string>
#include "Array.hpp"
#include "Config.hpp"
#include "Mesh.hpp"
#include "MetaFunc.hpp"

namespace cppkit
{
    class Station
    {
    public:
        Array<Real, 2> Coordinate;
        Array<Integer, 2> IndexInMesh;

        Station() = default;

        Station(Integer ns, Integer dim) : Coordinate(Array<Real, 2>(ns, dim))
        {
        }

        ~Station() {}

        inline Integer numOfSite() const noexcept
        {
            return Coordinate.length(1);
        }

        inline Integer dim() const noexcept { return Coordinate.length(2); }

        void setMeshIndex(const Mesh &mesh)
        {
            Integer ns = numOfSite(), ndim = dim();
            IndexInMesh = Array<Integer, 2>(ns, ndim);

            Array<Integer, 1> idt(ns);
            if (ndim >= 1)
            {
                idt = LocIndex_(mesh.Gz, Coordinate.col(ndim - 1));
                IndexInMesh.setCol(ndim - 1, idt);
            }
            if (ndim >= 2)
            {
                idt = LocIndex_(mesh.Gy, Coordinate.col(ndim - 2));
                IndexInMesh.setCol(ndim - 2, idt);
            }
            if (ndim >= 3)
            {
                idt = LocIndex_(mesh.Gx, Coordinate.col(ndim - 3));
                IndexInMesh.setCol(ndim - 3, idt);
            }
        }

        inline void import(std::istream &is)
        {
            std::string str;

            cppkit::getline(is, str);
            is >> Coordinate;
            cppkit::getline(is, str);
        }

        inline void import(std::string file)
        {
            std::fstream fs;
            fs.open(file, std::fstream::in);
            import(fs);
            fs.close();
        }

        friend std::ostream &operator<<(std::ostream &os, const Station &obj)
        {
            os << ">>STATION INFO" << std::endl;
            os << obj.Coordinate;
            os << ">>END STATION" << std::endl;

            return os;
        }

        friend std::istream &operator>>(std::istream &is, Station &obj)
        {
            obj.import(is);
            return is;
        }

    private:
        static Array<Integer, 1> LocIndex_(const Array<Real, 1> &gx,
                                           const Array<Real, 1> &xloc)
        {
            Integer nx = gx.size() - 1, ns = xloc.size();
            Array<Integer, 1> idx(ns);

            for (Integer i = 0; i < ns; ++i)
            {
                for (Integer j = 0; j < nx; ++j)
                {
                    if (xloc(i) >= gx(j) && xloc(i) < gx(j + 1))
                    {
                        idx(i) = j;
                        break;
                    }
                }
            }

            return idx;
        }
    };

}  // namespace cppkit
