
#pragma once

#include <type_traits>
#include "Complex.hpp"

namespace cppkit
{
    template<typename U>
    struct is_scalar
    {
        static constexpr bool value = std::is_arithmetic<U>::value;
    };

    template<typename U>
    struct is_scalar<complex<U>>
    {
        static constexpr bool value = std::is_arithmetic<U>::value;
    };

}  // namespace cppkit
