#pragma once

#include <iomanip>
#include <iostream>
#include <tuple>
#include <type_traits>
#include "Array.hpp"
#include "Complex.hpp"
#include "Config.hpp"
#include "Traits.hpp"

namespace cppkit
{
    template<typename T>
    class SparseVector
    {
    private:
        Integer _size;
        Integer *_index;
        T *_value;

        inline void alloc(Integer sz)
        {
            _index = new Integer[sz];
            _value = new T[sz];
            _size = sz;
        }

        inline void free()
        {
            delete[] _index;
            delete[] _value;
            _index = nullptr;
            _value = nullptr;
            _size = 0;
        }

        inline void copy(const SparseVector &other)
        {
            _size = other.size();
            if (_size > 0)
            {
                for (Integer i = 0; i < _size; ++i)
                {
                    _index[i] = other[i];
                    _value[i] = other(i);
                }
            }
        }

        inline void move(SparseVector &other)
        {
            _index = other.ptrIndex();
            _value = other.ptrValue();
            _size = other.size();
            other.nullify();
        }

    public:
        inline SparseVector() : _index(nullptr), _value(nullptr), _size(0) {}

        inline explicit SparseVector(Integer sz)
            : _index(nullptr), _value(nullptr), _size(0)
        {
            alloc(sz);
        }

        inline virtual ~SparseVector() { free(); }

        // copy constructor
        inline SparseVector(const SparseVector &other)
            : _value(nullptr), _index(nullptr), _size(0)
        {
            alloc(other.size());
            copy(other);
        }

        // assignment / copy assignment
        inline SparseVector &operator=(const SparseVector &other)
        {
            if (this != &other)
            {
                if (_size != other.size())
                {
                    free();
                    alloc(other.size());
                }
                copy(other);
            }
            return *this;
        }

        // move constructor
        inline SparseVector(SparseVector &&other)
            : _value(nullptr), _index(nullptr), _size(0)
        {
            move(other);
        }

        // move assignment
        inline SparseVector &operator=(SparseVector &&other)
        {
            if (this != &other)
            {
                free();
                move(other);
            }
            return *this;
        }

        // 一次元素赋值操作
        // e.g. sv = std::make_tuple(1, 3, 2.5)
        // e.g. sv = {1, 3, 2.5}
        inline SparseVector &operator=(
            const std::tuple<Integer, Integer, T> &elem)
        {
            _index[std::get<0>(elem)] = std::get<1>(elem);
            _value[std::get<0>(elem)] = std::get<2>(elem);
            return *this;
        }

        // 设置索引
        inline Integer &operator[](const Integer &i) noexcept
        {
            return _index[i];
        }
        // 获取索引
        inline const Integer &operator[](const Integer &i) const noexcept
        {
            return _index[i];
        }

        // 设置数值
        inline T &operator()(const Integer &i) noexcept { return _value[i]; }
        // 获取数值
        inline const T &operator()(const Integer &i) const noexcept
        {
            return _value[i];
        }

        // 缩放
        template<typename Scalar>
        inline
            typename std::enable_if_t<is_scalar<Scalar>::value, SparseVector &>
            operator*=(const Scalar &a)
        {
            if (!isEmpty())
            {
                for (Integer i = 0; i < _size; ++i)
                {
                    _value[i] *= a;
                }
            }
            return *this;
        }

        inline SparseVector resize(const Integer &newsize) const noexcept
        {
            SparseVector spv(newsize);

            Integer n = newsize > _size ? _size : newsize;
            for (Integer i = 0; i < n; ++i)
            {
                spv[i] = (*this)[i];
                spv(i) = (*this)(i);
            }

            return spv;
        }

        inline bool isEmpty() const noexcept { return !(_size > 0); }

        inline Integer size() const noexcept { return _size; }  // get nonzero
        inline void size(Integer sz) noexcept { _size = sz; }   // set nonzero

        inline Integer *ptrIndex() noexcept { return _index; }
        inline const Integer *ptrIndex() const noexcept { return _index; }

        inline T *ptrValue() noexcept { return _value; }
        inline const T *ptrValue() const noexcept { return _value; }

        inline void nullify()
        {
            _index = nullptr;
            _value = nullptr;
            _size = 0;
        }

        friend std::ostream &operator<<(std::ostream &os,
                                        const SparseVector &obj)
        {
            std::iostream::fmtflags oldflags(os.flags());

            os.setf(std::iostream::scientific | std::iostream::uppercase);

            Integer sz = obj.size();

            os << "  " << sz << std::endl;

            for (Integer i = 0; i < sz; ++i)
            {
                os << std::left << std::setw(8) << obj[i] << std::right
                   << std::setw(16) << std::setprecision(7) << obj(i)
                   << std::endl;
            }

            os.flags(oldflags);
            return os;
        }

        // 全向量与稀疏向量相乘
        template<typename U>
        inline friend auto operator*(const Vector<U> &v,
                                     const SparseVector &spv)
        {
            decltype(v(0) * spv(0)) r;
            r = 0.0;
            for (Integer i = 0; i < spv.size(); ++i)
            {
                r += spv(i) * v(spv[i]);
            }
            return r;
        }

        // 稀疏向量与全向量相乘
        template<typename U>
        inline friend auto operator*(const SparseVector &spv,
                                     const Vector<U> &v)
        {
            return v * spv;
        }

        // 稀疏向量加到一个全向量上
        template<typename U>
        inline friend Vector<U> &operator+=(Vector<U> &v,
                                            const SparseVector &spv)
        {
            for (Integer i = 0; i < spv.size(); ++i)
            {
                v(spv[i]) += spv(i);
            }
            return v;
        }
    };

    using IvSpVec = SparseVector<Integer>;  //包含整数值类型的稀疏向量
    using RvSpVec = SparseVector<Real>;  //包含实数值类型的稀疏向量
    using ZvSpVec = SparseVector<complex<Real>>;  //包含复数值类型的稀疏向量

}  // namespace cppkit
