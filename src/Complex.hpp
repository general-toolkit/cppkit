/** class template for complex numbers
 * cppkit::complex<T>, T can be intrinsic types, such as float, double, etc
 * std::complex library is too slow, bad performance
 */

#pragma once

#include <cmath>
#include <iomanip>
#include <iostream>
#include <string>
#include <type_traits>

namespace cppkit
{
    template<typename T>
    class complex
    {
    public:
        T real, imag;

        //**********************************************************************
        // constructor

        inline complex() : real(0.0), imag(0.0) {}

        template<typename U>
        inline complex(U re, U im) : real(re), imag(im)
        {
        }

        // copy constructor
        template<typename U>
        inline complex(const complex<U> &other) noexcept
            : real(other.real), imag(other.imag)
        {
        }

        // copy constructor from intrinsic types in C, such as 'float', 'double'
        template<typename U,
                 typename
                 = typename std::enable_if_t<std::is_arithmetic<U>::value>>
        inline complex(const U &a) noexcept : real(a), imag(0.0)
        {
        }

        // 负号
        inline complex operator-() const noexcept
        {
            return complex(-real, -imag);
        }

        // 正号
        inline complex operator+() const noexcept { return complex(*this); }

        // inline complex &operator=(const complex &rhs) noexcept
        // {
        //     if (this != &rhs)
        //     {
        //         real = rhs.real;
        //         imag = rhs.imag;
        //     }
        //     return *this;
        // }

        template<typename U>
        inline complex &operator=(const complex<U> &rhs) noexcept
        {
            real = rhs.real;
            imag = rhs.imag;
            return *this;
        }

        // assignment from intrinsic types in C, such as 'float', 'double'
        template<typename U>
        inline
            typename std::enable_if_t<std::is_arithmetic<U>::value, complex &>
            operator=(const U &rhs) noexcept
        {
            real = rhs;
            imag = 0.0;
            return *this;
        }

        //**********************************************************************

        template<typename U>
        inline complex &operator+=(const complex<U> &rhs) noexcept
        {
            real += rhs.real;
            imag += rhs.imag;
            return *this;
        }

        template<typename U>
        inline complex &operator-=(const complex<U> &rhs) noexcept
        {
            real -= rhs.real;
            imag -= rhs.imag;
            return *this;
        }

        template<typename U>
        inline complex &operator*=(const complex<U> &rhs) noexcept
        {
            using TR = decltype(real * rhs.real);
            TR a = real * rhs.real - imag * rhs.imag;
            TR b = real * rhs.imag + imag * rhs.real;
            real = a;
            imag = b;
            return *this;
        }

        template<typename U>
        inline complex &operator/=(const complex<U> &rhs)
        {
            using TR = decltype(real / rhs.real);
            TR r = 1.0 / rhs.norm();
            TR a = r * (real * rhs.real + imag * rhs.imag);
            TR b = r * (imag * rhs.real - real * rhs.imag);
            real = a;
            imag = b;
            return *this;
        }

        template<typename U>
        inline auto operator+(const complex<U> &rhs) const noexcept
        {
            return complex<decltype(real + rhs.real)>(real + rhs.real,
                                                      imag + rhs.imag);
        }

        template<typename U>
        inline auto operator-(const complex<U> &rhs) const noexcept
        {
            return complex<decltype(real - rhs.real)>(real - rhs.real,
                                                      imag - rhs.imag);
        }

        template<typename U>
        inline auto operator*(const complex<U> &rhs) const noexcept
        {
            using TR = decltype(real * rhs.real);
            TR a = real * rhs.real - imag * rhs.imag;
            TR b = real * rhs.imag + imag * rhs.real;
            return complex<TR>(a, b);
        }

        template<typename U>
        inline auto operator/(const complex<U> &rhs) const
        {
            using TR = decltype(real / rhs.real);
            TR r = 1.0 / rhs.norm();
            TR a = r * (real * rhs.real + imag * rhs.imag);
            TR b = r * (imag * rhs.real - real * rhs.imag);
            return complex<TR>(a, b);
        }

        //**********************************************************************

        template<typename U>
        inline
            typename std::enable_if_t<std::is_arithmetic<U>::value, complex &>
            operator+=(const U &rhs) noexcept
        {
            real += rhs;
            return *this;
        }

        template<typename U>
        inline
            typename std::enable_if_t<std::is_arithmetic<U>::value, complex &>
            operator-=(const U &rhs) noexcept
        {
            real -= rhs;
            return *this;
        }

        template<typename U>
        inline
            typename std::enable_if_t<std::is_arithmetic<U>::value, complex &>
            operator*=(const U &rhs) noexcept
        {
            real *= rhs;
            imag *= rhs;
            return *this;
        }

        template<typename U>
        inline
            typename std::enable_if_t<std::is_arithmetic<U>::value, complex &>
            operator/=(const U &rhs)
        {
            real /= rhs;
            imag /= rhs;
            return *this;
        }

        template<typename U,
                 typename
                 = typename std::enable_if_t<std::is_arithmetic<U>::value>>
        inline friend auto operator+(const complex &lhs, const U &rhs) noexcept
        {
            return complex<decltype(lhs.real + rhs)>(lhs.real + rhs, lhs.imag);
        }

        template<typename U,
                 typename
                 = typename std::enable_if_t<std::is_arithmetic<U>::value>>
        inline friend auto operator+(const U &lhs, const complex &rhs) noexcept
        {
            return complex<decltype(lhs + rhs.real)>(lhs + rhs.real, rhs.imag);
        }

        template<typename U,
                 typename
                 = typename std::enable_if_t<std::is_arithmetic<U>::value>>
        inline friend auto operator-(const complex &lhs, const U &rhs) noexcept
        {
            return complex<decltype(lhs.real - rhs)>(lhs.real - rhs, lhs.imag);
        }

        template<typename U,
                 typename
                 = typename std::enable_if_t<std::is_arithmetic<U>::value>>
        inline friend auto operator-(const U &lhs, const complex &rhs) noexcept
        {
            return complex<decltype(lhs - rhs.real)>(lhs - rhs.real, -rhs.imag);
        }

        template<typename U,
                 typename
                 = typename std::enable_if_t<std::is_arithmetic<U>::value>>
        inline friend auto operator*(const complex &lhs, const U &rhs) noexcept
        {
            return complex<decltype(lhs.real * rhs)>(lhs.real * rhs,
                                                     lhs.imag * rhs);
        }

        template<typename U,
                 typename
                 = typename std::enable_if_t<std::is_arithmetic<U>::value>>
        inline friend auto operator*(const U &lhs, const complex &rhs) noexcept
        {
            return complex<decltype(lhs * rhs.real)>(lhs * rhs.real,
                                                     lhs * rhs.imag);
        }

        template<typename U,
                 typename
                 = typename std::enable_if_t<std::is_arithmetic<U>::value>>
        inline friend auto operator/(const complex &lhs, const U &rhs)
        {
            return complex<decltype(lhs.real / rhs)>(lhs.real / rhs,
                                                     lhs.imag / rhs);
        }

        template<typename U,
                 typename
                 = typename std::enable_if_t<std::is_arithmetic<U>::value>>
        inline friend auto operator/(const U &lhs, const complex &rhs)
        {
            using TR = decltype(lhs / rhs.real);
            TR r = lhs / rhs.norm();
            return complex<TR>(r * rhs.real, -r * rhs.imag);
        }

        //**********************************************************************

        template<typename U>
        inline bool operator==(const complex<U> &rhs) const noexcept
        {
            return (real == rhs.real) && (imag == rhs.imag);
        }

        template<typename U>
        inline bool operator!=(const complex<U> &rhs) const noexcept
        {
            return !(*this == rhs);
        }

        template<typename U>
        inline std::enable_if_t<std::is_arithmetic<U>::value, bool> operator==(
            const U &rhs) const noexcept
        {
            return (real == rhs) && (imag == 0.0);
        }

        template<typename U>
        inline std::enable_if_t<std::is_arithmetic<U>::value, bool> operator!=(
            const U &rhs) const noexcept
        {
            return !(*this == rhs);
        }

        // 通过比较复数的模
        template<typename U>
        inline bool operator<(const complex<U> &rhs) const noexcept
        {
            return norm() < rhs.norm();
        }

        // 通过比较复数的模
        template<typename U>
        inline bool operator>(const complex<U> &rhs) const noexcept
        {
            return norm() > rhs.norm();
        }

        //**********************************************************************

        // 复数的模的平方
        inline T norm() const noexcept { return real * real + imag * imag; }

        // 复数的模
        inline T abs() const noexcept { return std::sqrt(norm()); }

        // 复数的幅角
        inline T arg() const noexcept { return std::atan2(imag, real); }

        // 共轭复数
        inline complex conj() const noexcept { return complex(real, -imag); }

        std::string format() const noexcept
        {
            std::string fmtStr = "(";
            fmtStr.append(std::to_string(real));
            if (imag < 0)
            {
                fmtStr.append(" - ");
                fmtStr.append(std::to_string(-imag));
            }
            else
            {
                fmtStr.append(" + ");
                fmtStr.append(std::to_string(imag));
            }
            fmtStr.append("i)");

            return fmtStr;
        }

        //**********************************************************************

        friend std::ostream &operator<<(std::ostream &out, const complex &obj)
        {
            std::iostream::fmtflags oldflag(out.flags());
            out.setf(std::iostream::right | std::iostream::scientific
                     | std::iostream::uppercase);

            out << std::setw(16) << std::setprecision(7) << obj.real
                << std::setw(16) << std::setprecision(7) << obj.imag;

            out.flags(oldflag);
            return out;
        }

        friend std::istream &operator>>(std::istream &in, complex &obj)
        {
            in >> obj.real >> obj.imag;
            return in;
        }

        //**********************************************************************

        ~complex() {}
    };


#pragma omp declare reduction (+ : cppkit::complex<double> : omp_out += omp_in) initializer (omp_priv(omp_orig))
#pragma omp declare reduction (+ : cppkit::complex<float> : omp_out += omp_in) initializer (omp_priv(omp_orig))

}  // namespace cppkit
