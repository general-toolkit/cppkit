/** 稀疏矩阵
 * 存储格式: CSR
 * 添加一个新的数组，指示主对角线上的元素的位置，以便于矩阵分解等
 */


#pragma once

#include "Array.hpp"
#include "Config.hpp"
#include "MetaFunc.hpp"
#include "Operator.hpp"
#include "VectorExpression.hpp"

namespace cppkit
{
    enum class DecomposeMethod : int
    {
        ILU0 = 0,
        ILUP = 1,
        ILUT = 2,
        DEFAULT = ILU0
    };

    template<typename T>
    class SparseMatrix
    {
    private:
        Integer _size;       // 非0元素占用的内存量
        Integer _n;          // 矩阵的大小
        T *_value;           // 非0元素值
        Integer *_rowptr;    // row pointer
        Integer *_colindex;  // column index
        Integer *_diagloc;   // diagonal element location

        inline void copy(const SparseMatrix &other)
        {
            Integer i;

#pragma omp parallel for
            for (i = 0; i < _n; ++i)
            {
                _rowptr[i] = other.row(i);
                _diagloc[i] = other.diag(i);
            }
            if (_n > 0) _rowptr[_n] = other.row(_n);

#pragma omp parallel for
            for (i = 0; i < _size; ++i)
            {
                _colindex[i] = other.col(i);
                _value[i] = other.value(i);
            }
        }

        inline void nullify()
        {
            _value = nullptr;
            _rowptr = nullptr;
            _colindex = nullptr;
            _diagloc = nullptr;
            _n = 0;
            _size = 0;
        }

        inline void move(SparseMatrix &other)
        {
            _n = other.rank();
            _size = other.size();
            _value = other.value();
            _rowptr = other.row();
            _colindex = other.col();
            _diagloc = other.diag();
            other.nullify();
        }

        inline void alloc(Integer n, Integer sz)
        {
            _value = new T[sz];
            _rowptr = new Integer[n + 1];
            _colindex = new Integer[sz];
            _diagloc = new Integer[n];
            _n = n;
            _size = sz;
        }

        inline void free()
        {
            delete[] _value;
            delete[] _rowptr;
            delete[] _colindex;
            delete[] _diagloc;
            _value = nullptr;
            _rowptr = nullptr;
            _colindex = nullptr;
            _diagloc = nullptr;
            _size = _n = 0;
        }

    public:
        inline SparseMatrix()
            : _value(nullptr)
            , _rowptr(nullptr)
            , _colindex(nullptr)
            , _diagloc(nullptr)
            , _n(0)
            , _size(0)
        {
        }

        inline SparseMatrix(Integer n, Integer sz)
            : _value(new T[sz])
            , _rowptr(new Integer[n + 1])
            , _colindex(new Integer[sz])
            , _diagloc(new Integer[n])
            , _n(n)
            , _size(sz)
        {
            _rowptr[0] = 0;
        }

        inline virtual ~SparseMatrix() { free(); }

        inline SparseMatrix(const SparseMatrix &other) : SparseMatrix()
        {
            alloc(other.rank(), other.size());
            copy(other);
        }

        inline SparseMatrix &operator=(const SparseMatrix &rhs)
        {
            if (this != &rhs)
            {
                free();
                alloc(rhs.rank(), rhs.size());
                copy(rhs);
            }
            return *this;
        }

        inline SparseMatrix(SparseMatrix &&other) : SparseMatrix()
        {
            move(other);
        }

        inline SparseMatrix &operator=(SparseMatrix &&rhs)
        {
            if (this != &rhs)
            {
                free();
                move(rhs);
            }
            return *this;
        }

        inline Integer rank() const noexcept { return _n; }

        inline Integer size() const noexcept { return _size; }

        inline T *value() noexcept { return _value; }
        inline const T *value() const noexcept { return _value; }
        inline T &value(const Integer &idx) { return _value[idx]; }
        inline const T &value(const Integer &idx) const { return _value[idx]; }

        inline Integer *row() noexcept { return _rowptr; }
        inline const Integer *row() const noexcept { return _rowptr; }
        inline Integer &row(const Integer &idx) { return _rowptr[idx]; }
        inline const Integer &row(const Integer &idx) const
        {
            return _rowptr[idx];
        }

        inline Integer *col() noexcept { return _colindex; }
        inline const Integer *col() const noexcept { return _colindex; }
        inline Integer &col(const Integer &idx) { return _colindex[idx]; }
        inline const Integer &col(const Integer &idx) const
        {
            return _colindex[idx];
        }

        inline Integer *diag() noexcept { return _diagloc; }
        inline const Integer *diag() const noexcept { return _diagloc; }
        inline Integer &diag(const Integer &idx) { return _diagloc[idx]; }
        inline const Integer &diag(const Integer &idx) const
        {
            return _diagloc[idx];
        }

        inline Array<T, 2> denseMatrix() const
        {
            Array<T, 2> A(_n, _n);
            A = 0.0;
            for (Integer i = 0; i < _n; ++i)
            {
                for (Integer j = row(i); j < row(i + 1); ++j)
                {
                    A(i, col(j)) = value(j);
                }
            }
            return A;
        }

        template<typename Ux, typename Uy>
        inline void multiplyVector(const Vector<Ux> &x, Vector<Uy> &y) const
        {
#pragma omp parallel for
            for (Integer i = 0; i < _n; ++i)
            {
                y(i) = 0.0;
                for (Integer j = row(i); j < row(i + 1); ++j)
                {
                    y(i) += value(j) * x(col(j));
                }
            }
        }

        template<typename Ux, typename Uy>
        inline void transMultiplyVector(const Vector<Ux> &x,
                                        Vector<Uy> &y) const
        {
            // 假定矩阵是对称矩阵
            multiplyVector(x, y);
        }

        template<typename Ux, typename Uy>
        inline void solveLower(Vector<Ux> &x, const Vector<Uy> &y) const
        {
            decltype(Ux() * T()) t;
            for (Integer i = 0; i < _n; ++i)
            {
                t = 0.0;
                for (Integer j = row(i); j < diag(i); ++j)
                {
                    t += value(j) * x(col(j));
                }
                x(i) = y(i) - t;  // 单位下三角矩阵
            }
        }

        template<typename Ux, typename Uy>
        inline void solveUpper(Vector<Ux> &x, const Vector<Uy> &y) const
        {
            decltype(Ux() * T()) t;
            for (Integer i = _n - 1; i >= 0; --i)
            {
                t = 0.0;
                for (Integer j = diag(i) + 1; j < row(i + 1); ++j)
                {
                    t += value(j) * x(col(j));
                }
                x(i) = (y(i) - t) / value(diag(i));
            }
        }

        template<typename Ux, typename Uy>
        inline void solve(Vector<Ux> &x, const Vector<Uy> &y) const
        {
            solveLower(x, y);
            for (Integer i = _n - 1; i >= 0; --i)
            {
                for (Integer j = diag(i) + 1; j < row(i + 1); ++j)
                {
                    x(i) -= value(j) * x(col(j));
                }
                x(i) /= value(diag(i));
            }
        }

        template<typename Ux, typename Uy>
        inline void solveTransLower(Vector<Ux> &x, const Vector<Uy> &y) const
        {
            x = y;
            for (Integer i = _n - 1; i >= 0; --i)
            {
                for (Integer j = row(i); j < diag(i); ++j)
                {
                    x(col(j)) -= x(i) * cppkit::conj(value(j));
                }
            }
        }

        template<typename Ux, typename Uy>
        inline void solveTransUpper(Vector<Ux> &x, const Vector<Uy> &y) const
        {
            x = y;
            for (Integer i = 0; i < _n; ++i)
            {
                x(i) /= cppkit::conj(value(diag(i)));
                for (Integer j = diag(i) + 1; j < row(i + 1); ++j)
                {
                    x(col(j)) -= x(i) * cppkit::conj(value(j));
                }
            }
        }


        /**
         * @brief ILU(0)算法伪代码
         *          for i = 2, n
         *              for k = 1, i-1              // (i,k) be NNZ(A)
         *                  a(i,k) = a(i,k)/a(k,k)
         *                  for j = k+1, n          // (i,j) be NNZ(A)
         *                      a(i,j) = a(i,j) - a(i,k)*a(k,j)
         *                  end
         *              end
         *          end
         */
        template<typename U>
        void ilu0(SparseMatrix<U> &M) const
        {
            Vector<Integer> iw(_n);
            iw = 0;

            Integer irow, j, k, krow, jw;
            for (irow = 0; irow < _n; ++irow)
            {
                M.row(irow) = row(irow);
                M.row(irow + 1) = row(irow + 1);
                M.diag(irow) = diag(irow);
                for (j = row(irow); j < row(irow + 1); ++j)
                {
                    /* iw向量中非0值的下标，表示压缩矩阵中irow行的非0元素的列下标
                    iw向量中非0值的具体数值，表示矩阵非0元素在压缩格式中的内存索引位置
                     */
                    iw(col(j)) = j;

                    M.col(j) = col(j);
                    M.value(j) = value(j);
                }

                for (k = row(irow); k < diag(irow); ++k)
                {
                    krow = col(k);
                    M.value(k) /= M.value(diag(krow));
                    for (j = diag(krow) + 1; j < row(krow + 1); ++j)
                    {
                        jw = iw(col(j));
                        if (jw != 0) M.value(jw) -= M.value(k) * M.value(j);
                    }
                }

                for (j = row(irow); j < row(irow + 1); ++j)
                {
                    iw(col(j)) = 0;
                }
            }
        }

        template<typename U>
        void decompose(SparseMatrix<U> &M,
                       DecomposeMethod method = DecomposeMethod::DEFAULT) const
        {
            switch (method)
            {
                case DecomposeMethod::ILU0:
                    ilu0(M);
                    break;
                default:
                    break;
            }
        }
    };


    template<typename T, typename RightExpr>
    class SpmatVectorExpression
        : public VectorExpression<SpmatVectorExpression<T, RightExpr>>
    {
        // 稀疏矩阵与向量相乘的表达式 A*x
    private:
        const SparseMatrix<T> &_mat;
        const RightExpr &_vec;

    public:
        using value_type = decltype(
            OpMultiply::apply(T(), typename RightExpr::value_type()));

        inline Integer size() const { return _mat.rank(); }

        inline value_type operator[](const Integer &irow) const
        {
            value_type t = 0.0;
            for (Integer j = _mat.row(irow); j < _mat.row(irow + 1); ++j)
            {
                t += _mat.value(j) * _vec[_mat.col(j)];
            }
            return t;
        }

        inline SpmatVectorExpression(const SparseMatrix<T> &lhs,
                                     const VectorExpression<RightExpr> &rhs)
            : _mat(lhs), _vec(rhs)
        {
        }
    };

    template<typename T, typename RightExpr>
    inline auto operator*(const SparseMatrix<T> &lhs,
                          const VectorExpression<RightExpr> &rhs)
    {
        using TR = SpmatVectorExpression<T, RightExpr>;
        return TR(lhs, rhs);
    }

}  // namespace cppkit
