// clang-format off
/** 代码块运行时间的测量，Need c++11 support
 *      1) 定义一个Clock类对象，Clock clock;
 *      2) 在代码块运行之前，调用clock.tic()函数
 *      3) 代码块运行完之后，调用clock.toc(TimeUnit)函数返回运行时间，可以指定时间单位
 */
// clang-format on

#pragma once

#include <chrono>

namespace cppkit
{
    class Clock
    {
    public:
        enum class TimeUnit : int
        {
            InSecond = 0,
            InMinute = 1,
            InHour = 2,
            Default = InSecond
        };

        inline Clock() : valid(false){};

        inline ~Clock(){};

        inline void tic()
        {
            time_begin = std::chrono::steady_clock::now();
            valid = true;
        }

        inline double toc(TimeUnit unit = TimeUnit::InSecond)
        {
            if (valid)
            {
                const std::chrono::steady_clock::time_point time_end
                    = std::chrono::steady_clock::now();

                typedef std::chrono::duration<double> duration_type;

                const duration_type chrono_span
                    = std::chrono::duration_cast<duration_type>(time_end
                                                                - time_begin);
                double time_elapsed = chrono_span.count();

                switch (unit)
                {
                    case TimeUnit::InMinute:
                        time_elapsed /= 60.0;
                        break;
                    case TimeUnit::InHour:
                        time_elapsed /= 3600.0;
                        break;
                    default:
                        break;
                }
                return time_elapsed;
            }
            else
            {
                return 0.0;
            }
        }

    private:
        std::chrono::steady_clock::time_point time_begin;
        bool valid;
    };

}  // namespace cppkit
