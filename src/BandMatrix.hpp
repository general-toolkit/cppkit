
#pragma once

#include <iostream>
#include "Array.hpp"
#include "Config.hpp"
#include "MetaFunc.hpp"


namespace cppkit
{
    /** 带状矩阵类
     * 原始矩阵
     *      a11 a12 a13   *   *
     *      a21 a22 a23 a24  *
     *       *  a32 a33 a34 a35
     *       *   *  a43 a44 a45
     *       *   *   *  a54 a55
     *
     * ml = 1, mu = 2, mm = 4
     *
     * 压缩后的矩阵 (保持行索引)
     *      *  a11 a12 a13
     *     a21 a22 a23 a24
     *     a32 a33 a34 a35
     *     a43 a44 a45  *
     *     a54 a55  *   *
     *
     * 为了于C语言一致，过程中的索引都从 0 开始
     *
     */
    template<typename T>
    class BandMatrix
    {
    private:
        Vector<Integer> piv;  // pivot
        Integer ml, mu, mm;
        Integer n;
        bool decomposed = false;
        Array<T, 2> L;
        Array<T, 2> AU;  // 同时存储系数矩阵和分解后的U矩阵

        inline void decompose()
        {
            const T TINY = 1.0e-40;
            Integer i, j, k, l = ml;
            T dum;

            for (i = 0; i < ml; ++i)
            {
                for (j = ml - i; j < mm; ++j)
                {
                    AU(i, j - l) = AU(i, j);
                }
                --l;
                for (j = mm - l - 1; j < mm; ++j)
                {
                    AU(i, j) = 0.0;
                }
            }

            l = ml;
            for (k = 0; k < n; ++k)
            {
                dum = AU(k, 0);
                i = k;
                if (l < n) ++l;

                for (j = k + 1; j < l; ++j)
                {
                    if (cppkit::abs(AU(j, 0)) > cppkit::abs(dum))
                    {
                        dum = AU(j, 0);
                        i = j;
                    }
                }
                piv(k) = i;

                if (dum == 0.0)
                {
                    std::cout << "Warning: zero pivot!!!" << std::endl;
                    AU(k, 0) = TINY;
                }

                if (i != k)
                {
                    for (j = 0; j < mm; ++j)
                    {
                        cppkit::swap(AU(k, j), AU(i, j));
                    }
                }

                for (i = k + 1; i < l; ++i)
                {
                    dum = AU(i, 0) / AU(k, 0);
                    L(k, i - k - 1) = dum;
                    for (j = 1; j < mm; ++j)
                    {
                        AU(i, j - 1) = AU(i, j) - dum * AU(k, j);
                    }
                    AU(i, mm - 1) = 0.0;
                }
            }

            decomposed = true;
        }

        template<typename Tx, typename Tb>
        inline void permuteCopy(Vector<Tx> &x, const Vector<Tb> &b)
        {
            for (Integer i = 0; i < b.size(); ++i)
            {
                x(i) = b(piv(i));
            }
        }

        // 压缩后的带状矩阵的列索引转化为原始矩阵的列索引
        inline Integer originColIndex(const Integer &irowBand,
                                      const Integer &jcolBand) noexcept
        {
            return (jcolBand + irowBand - ml);
        }

        // 原始矩阵的列索引转化为压缩后的带状矩阵的列索引
        inline Integer bandColIndex(const Integer &irow,
                                    const Integer &jcol) noexcept
        {
            return (jcol - irow + ml);
        }

    public:
        BandMatrix() = default;
        ~BandMatrix() {}
        BandMatrix(const BandMatrix &other) = default;
        BandMatrix &operator=(const BandMatrix &rhs) = default;
        BandMatrix(BandMatrix &&other) = default;
        BandMatrix &operator=(BandMatrix &&rhs) = default;

        BandMatrix(Integer nrank, Integer lWidth, Integer uWidth)
            : ml(lWidth)
            , mu(uWidth)
            , mm(lWidth + uWidth + 1)
            , decomposed(false)
            , n(nrank)
            , piv(Vector<Integer>(nrank))
            , AU(Array<T, 2>(nrank, lWidth + uWidth + 1))
            , L(Array<T, 2>(nrank, lWidth))
        {
        }

        inline T &operator()(const Integer &i, const Integer &j)
        {
            return AU(i, j);
        }

        inline const T &operator()(const Integer &i, const Integer &j) const
        {
            return AU(i, j);
        }

        inline void reset() noexcept { decomposed = false; }

        template<typename Tx, typename Tb>
        void solve(Vector<Tx> &x, const Vector<Tb> &b)
        {
            if (!decomposed) decompose();

            Integer i, j, k, l = ml;
            decltype(T() * Tx()) dum;

            for (k = 0; k < n; ++k)
            {
                x(k) = b(k);
            }

            for (k = 0; k < n; ++k)
            {
                j = piv(k);
                if (j != k) cppkit::swap(x(k), x(j));

                if (l < n) ++l;
                for (j = k + 1; j < l; ++j)
                {
                    x(j) -= L(k, j - k - 1) * x(k);
                }
            }

            l = 1;
            for (i = n - 1; i >= 0; --i)
            {
                dum = x(i);
                for (k = 1; k < l; ++k)
                {
                    dum -= AU(i, k) * x(k + i);
                }
                x(i) = dum / AU(i, 0);
                if (l < mm) ++l;
            }
        }
    };

}  // namespace cppkit
