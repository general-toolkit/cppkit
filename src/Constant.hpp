#pragma once
#include "Config.hpp"

namespace cppkit
{
    constexpr long double HIGH_PI
        = 3.14159265358979323846264338327950288419716939937510582097494459230781640628620899862803482534211706798L;

    // 圆周率
    constexpr Real PI = static_cast<Real>(HIGH_PI);

    constexpr Real MU0 = 4.0L * HIGH_PI * 1.0E-7L;  // 真空中的磁导率

    constexpr Real AIR_COND = 1.0E-8;  // 空气的电导率

    constexpr Real RATIO_RAD2DEG
        = 180.0L / HIGH_PI;  // 弧度转化为角度的转换因子
    constexpr Real RATIO_DEG2RAD
        = HIGH_PI / 180.0L;  // 角度转化为弧度的转换因子
}  // namespace cppkit
