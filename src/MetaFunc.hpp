// 实现一些基本函数

#pragma once

#include <cmath>
#include <iostream>
#include <string>
#include "Complex.hpp"
#include "Constant.hpp"

namespace cppkit
{
    /** 处理不同的line endings
     * CrLf: \r\n
     * Lf: \n
     */
    inline std::istream &getline(std::istream &is, std::string &line)
    {
        std::getline(is, line);  // 如果是CrLf结尾，那么'\r'也会读进来
        if (line.back() == '\r') line.erase(line.length() - 1, 1);
        return is;
    }

    // 去除字符串首尾空白
    inline std::string trim(const std::string &s)
    {
        const std::string WHITESPACE = " \n\r\t";
        std::string trimStr;

        // trim leading spaces
        size_t start = s.find_first_not_of(WHITESPACE);
        trimStr = (start == std::string::npos ? "" : s.substr(start));

        // trim ending spaces
        size_t end = trimStr.find_last_not_of(WHITESPACE);
        trimStr = (end == std::string::npos ? "" : trimStr.substr(0, end + 1));

        return trimStr;
    }

    template<typename T>
    inline void swap(T &a, T &b) noexcept
    {
        T t = a;
        a = b;
        b = t;
    }

    template<typename T>
    inline T min(const T &a, const T &b) noexcept
    {
        return a < b ? a : b;
    }

    template<typename T>
    inline T max(const T &a, const T &b) noexcept
    {
        return a > b ? a : b;
    }

    template<typename T>
    inline T deg2Rad(const T &deg) noexcept
    {
        return deg * RATIO_DEG2RAD;
    }

    template<typename T>
    inline T rad2deg(const T &rad) noexcept
    {
        return rad * RATIO_RAD2DEG;
    }

    template<typename T>
    inline T conj(const T &a) noexcept
    {
        return a;
    }

    template<typename T>
    inline complex<T> conj(const complex<T> &z) noexcept
    {
        return z.conj();
    }

    template<typename T>
    inline T abs(const T &a) noexcept
    {
        return a < 0 ? -a : a;
    }

    template<typename T>
    inline T abs(const complex<T> &z) noexcept
    {
        return z.abs();
    }

    template<typename T>
    inline T sqrt(const T &a) noexcept
    {
        return std::sqrt(a);
    }

    template<typename T>
    inline T sqrt(const complex<T> &a) noexcept
    {
        return a.abs();
    }

}  // namespace cppkit
