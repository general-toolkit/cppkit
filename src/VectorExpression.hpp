// https://stackoverflow.com/questions/35613620/how-should-i-use-expression-templates-in-order-to-implement-scalar-multiplicatio

#pragma once

#include <type_traits>
#include "Config.hpp"
#include "Operator.hpp"
#include "Traits.hpp"

namespace cppkit
{
    // curiously recurring template pattern (CRTP)
    template<typename SubType>  // SubType是子类类型
    class VectorExpression
    {
    public:
        inline Integer size() const
        {
            return static_cast<const SubType &>(*this).size();
        }

        inline auto operator[](const Integer &i) const
        {
            return static_cast<const SubType &>(*this)[i];  // 推迟到子类
        }

        inline operator SubType &() { return static_cast<SubType &>(*this); }

        inline operator const SubType &() const
        {
            return static_cast<const SubType &>(*this);  // 推迟到子类
            // return *static_cast<const SubType *>(this);
        }
    };

    //**************************************************************************
    template<typename LeftExpr, typename RightExpr, typename BinaryOperator>
    class VectorBinaryExpression
        : public VectorExpression<
              VectorBinaryExpression<LeftExpr, RightExpr, BinaryOperator>>
    {
        // 两个向量表达式的二元运算，如加、减法
        static_assert(is_binary_operator<BinaryOperator>::value,
                      "Binary Operator needed");

    private:
        const LeftExpr &_lhs;
        const RightExpr &_rhs;

    public:
        using value_type = decltype(BinaryOperator::apply(
            typename LeftExpr::value_type(), typename RightExpr::value_type()));

        inline VectorBinaryExpression(const VectorExpression<LeftExpr> &lhs,
                                      const VectorExpression<RightExpr> &rhs)
            : _lhs(lhs), _rhs(rhs)
        {
        }

        inline Integer size() const { return _lhs.size(); }

        inline value_type operator[](const Integer &i) const
        {
            return BinaryOperator::apply(_lhs[i], _rhs[i]);
        }
    };

    // 向量相关的加法表达式
    template<typename LeftExpr, typename RightExpr>
    inline auto operator+(const VectorExpression<LeftExpr> &lhs,
                          const VectorExpression<RightExpr> &rhs)
    {
        using TR = VectorBinaryExpression<LeftExpr, RightExpr, OpPlus>;
        return TR(lhs, rhs);
    }

    template<typename LeftExpr, typename RightExpr>
    inline auto operator-(const VectorExpression<LeftExpr> &lhs,
                          const VectorExpression<RightExpr> &rhs)
    {
        using TR = VectorBinaryExpression<LeftExpr, RightExpr, OpMinus>;
        return TR(lhs, rhs);
    }

    template<typename LeftExpr, typename RightExpr>
    inline auto operator*(const VectorExpression<LeftExpr> &lhs,
                          const VectorExpression<RightExpr> &rhs)
    {
        using TR = VectorBinaryExpression<LeftExpr, RightExpr, OpMultiply>;
        return TR(lhs, rhs);
    }

    template<typename LeftExpr, typename RightExpr>
    inline auto operator/(const VectorExpression<LeftExpr> &lhs,
                          const VectorExpression<RightExpr> &rhs)
    {
        using TR = VectorBinaryExpression<LeftExpr, RightExpr, OpDivide>;
        return TR(lhs, rhs);
    }


    //**************************************************************************
    template<typename Scalar, typename Expr, typename BinaryOperator>
    class ScalarVectorExpression
        : public VectorExpression<
              ScalarVectorExpression<Scalar, Expr, BinaryOperator>>
    {
        // 标量（左操作数）和向量（右操作数）的运算表达式

        static_assert(is_binary_operator<BinaryOperator>::value,
                      "Binary Operator needed");

    private:
        const Scalar _scalar;
        const Expr &_expr;

    public:
        using value_type
            = decltype(BinaryOperator::apply(Scalar(), Expr::value_type()));

        inline ScalarVectorExpression(const Scalar &lhs,
                                      const VectorExpression<Expr> &rhs)
            : _scalar(lhs), _expr(rhs)
        {
        }

        inline Integer size() const { return _expr.size(); }

        inline value_type operator[](const Integer &i) const
        {
            return BinaryOperator::apply(_scalar, _expr[i]);
        }
    };

    template<typename Scalar, typename RightExpr>
    inline typename std::enable_if_t<
        is_scalar<Scalar>::value,
        ScalarVectorExpression<Scalar, RightExpr, OpPlus>>
        operator+(const Scalar &lhs, const VectorExpression<RightExpr> &rhs)
    {
        using TR = ScalarVectorExpression<Scalar, RightExpr, OpPlus>;
        return TR(lhs, rhs);
    }

    template<typename Scalar, typename RightExpr>
    inline typename std::enable_if_t<
        is_scalar<Scalar>::value,
        ScalarVectorExpression<Scalar, RightExpr, OpMinus>>
        operator-(const Scalar &lhs, const VectorExpression<RightExpr> &rhs)
    {
        using TR = ScalarVectorExpression<Scalar, RightExpr, OpMinus>;
        return TR(lhs, rhs);
    }

    template<typename Scalar, typename RightExpr>
    inline typename std::enable_if_t<
        is_scalar<Scalar>::value,
        ScalarVectorExpression<Scalar, RightExpr, OpMultiply>>
        operator*(const Scalar &lhs, const VectorExpression<RightExpr> &rhs)
    {
        using TR = ScalarVectorExpression<Scalar, RightExpr, OpMultiply>;
        return TR(lhs, rhs);
    }

    template<typename Scalar, typename RightExpr>
    inline typename std::enable_if_t<
        is_scalar<Scalar>::value,
        ScalarVectorExpression<Scalar, RightExpr, OpDivide>>
        operator/(const Scalar &lhs, const VectorExpression<RightExpr> &rhs)
    {
        using TR = ScalarVectorExpression<Scalar, RightExpr, OpDivide>;
        return TR(lhs, rhs);
    }


    //**************************************************************************
    template<typename Scalar, typename Expr, typename BinaryOperator>
    class VectorScalarExpression
        : public VectorExpression<
              VectorScalarExpression<Scalar, Expr, BinaryOperator>>
    {
        // 向量（左操作数）和标量（右操作数）的运算表达式

        static_assert(is_binary_operator<BinaryOperator>::value,
                      "Binary Operator needed");

    private:
        const Scalar _scalar;
        const Expr &_expr;

    public:
        using value_type
            = decltype(BinaryOperator::apply(Scalar(), Expr::value_type()));

        inline VectorScalarExpression(const VectorExpression<Expr> &lhs,
                                      const Scalar &rhs)
            : _scalar(rhs), _expr(lhs)
        {
        }

        inline Integer size() const { return _expr.size(); }

        inline value_type operator[](const Integer &i) const
        {
            return BinaryOperator::apply(_expr[i], _scalar);
        }
    };

    template<typename Scalar, typename LeftExpr>
    inline typename std::enable_if_t<
        is_scalar<Scalar>::value,
        VectorScalarExpression<Scalar, LeftExpr, OpPlus>>
        operator+(const VectorExpression<LeftExpr> &lhs, const Scalar &rhs)
    {
        using TR = VectorScalarExpression<Scalar, LeftExpr, OpPlus>;
        return TR(lhs, rhs);
    }

    template<typename Scalar, typename LeftExpr>
    inline typename std::enable_if_t<
        is_scalar<Scalar>::value,
        VectorScalarExpression<Scalar, LeftExpr, OpMinus>>
        operator-(const VectorExpression<LeftExpr> &lhs, const Scalar &rhs)
    {
        using TR = VectorScalarExpression<Scalar, LeftExpr, OpMinus>;
        return TR(lhs, rhs);
    }

    template<typename Scalar, typename LeftExpr>
    inline typename std::enable_if_t<
        is_scalar<Scalar>::value,
        VectorScalarExpression<Scalar, LeftExpr, OpMultiply>>
        operator*(const VectorExpression<LeftExpr> &lhs, const Scalar &rhs)
    {
        using TR = VectorScalarExpression<Scalar, LeftExpr, OpMultiply>;
        return TR(lhs, rhs);
    }

    template<typename Scalar, typename LeftExpr>
    inline typename std::enable_if_t<
        is_scalar<Scalar>::value,
        VectorScalarExpression<Scalar, LeftExpr, OpDivide>>
        operator/(const VectorExpression<LeftExpr> &lhs, const Scalar &rhs)
    {
        using TR = VectorScalarExpression<Scalar, LeftExpr, OpDivide>;
        return TR(lhs, rhs);
    }

}  // namespace cppkit
