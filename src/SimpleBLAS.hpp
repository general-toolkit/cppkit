#pragma once

#include <cmath>
#include "Array.hpp"
#include "Complex.hpp"
#include "Config.hpp"
#include "MetaFunc.hpp"

namespace cppkit
{
    template<typename T>
    inline T dot(const Vector<T> &x, const Vector<T> &y)
    {
        Integer n = x.size();
        T result = 0.0;

#pragma omp parallel for reduction(+ : result)
        for (Integer i = 0; i < n; ++i)
        {
            result += x(i) * y(i);
        }
        return result;
    }

    template<typename T>
    inline T dotc(const Vector<T> &x, const Vector<T> &y)
    {
        return cppkit::dot(x, y);
    }

    template<typename T>
    inline cppkit::complex<T> dotc(const Vector<cppkit::complex<T>> &x,
                                   const Vector<cppkit::complex<T>> &y)
    {
        Integer n = x.size();
        cppkit::complex<T> result = cppkit::complex<T>(0.0, 0.0);

#pragma omp parallel for reduction(+ : result)
        for (Integer i = 0; i < n; ++i)
        {
            result += cppkit::conj(x(i)) * y(i);
        }
        return result;
    }

    template<typename T>
    inline T norm(const Vector<T> &x)
    {
        return std::sqrt(dot(x, x));
    }

    template<typename T>
    inline T norm(const Vector<cppkit::complex<T>> &x)
    {
        Integer n = x.size();

        T result = 0.0;
#pragma omp parallel for reduction(+ : result)
        for (Integer i = 0; i < n; ++i)
        {
            result += x(i).norm();
        }
        return std::sqrt(result);
    }

}  // namespace cppkit
