/** A simple multi-dimensional dynamic array class
 *Restriction: (1) 0-based indexing
               (2) row-major order
 *Notes:
    need C++14 support for compilation: -std=c++14
 *Example:
    Array<double,2>: 2-dimensional double array
    Array<float,1>:  1-dimensional vector
 */

#pragma once

#include <algorithm>
#include <array>
#include <functional>
#include <iomanip>
#include <iostream>
#include <type_traits>
#include <vector>
#include "Config.hpp"
#include "Traits.hpp"
#include "VectorExpression.hpp"


namespace cppkit
{
    template<typename T, Integer RANK>
    class Array : public VectorExpression<Array<T, RANK>>
    {
        using sizeArray = std::array<Integer, RANK>;

    private:
        T *_data;
        Integer _size;
        sizeArray _length, _stride;

        inline void alloc(Integer sz)
        {
            if (sz > 0)
            {
                _data = new T[sz];
                _size = sz;
            }
        }

        inline void free()
        {
            delete[] _data;
            _data = nullptr;
            _size = 0;
        }

        inline void setStride()
        {
            _stride[RANK - 1] = 1;
            for (int i = RANK - 2; i >= 0; --i)
            {
                _stride[i] = _length[i + 1] * _stride[i + 1];
            }
        }

        inline void copy(const Array &other)
        {
            length(other.length());
            setStride();
            if (_size > 0) { std::copy_n(other.rawData(), _size, _data); }
        }

        inline void fastCopy(const Array &other)
        {
            length(other.length());
            setStride();
            if (_size > 0)
            {
#pragma omp parallel for
                for (Integer i = 0; i < _size; ++i)
                {
                    _data[i] = other[i];
                }
            }
        }

        inline void move(Array &other)
        {
            _data = other.rawData();
            _size = other.size();
            length(other.length());
            setStride();
            other.nullify();
        }

        template<typename firstIndex, typename... restIndices>
        inline Integer rawIndex(const firstIndex &i,
                                const restIndices &... rest) const
        {
            static_assert(std::is_integral<firstIndex>::value,
                          "Array indices must be integer");
            return static_cast<Integer>(
                _stride[RANK - 1 - (sizeof...(restIndices))] * i
                + rawIndex(rest...));
        }

        template<typename lastIndex>
        inline Integer rawIndex(const lastIndex &i) const
        {
            static_assert(std::is_integral<lastIndex>::value,
                          "Array indices must be integer");
            return static_cast<Integer>(i * _stride[RANK - 1]);
        }

        template<typename firstDim, typename... Args>
        inline void computeLength(firstDim len, Args... args)
        {
            static_assert(std::is_integral<firstDim>::value,
                          "Array length must be integer");
            _length[RANK - 1 - (sizeof...(args))] = len;
            computeLength(args...);
        }

        template<typename lastDim>
        inline void computeLength(lastDim len)
        {
            static_assert(std::is_integral<lastDim>::value,
                          "Array length must be integer");
            _length[RANK - 1] = len;
        }

    public:
        using value_type = T;

        inline Array() : _data(nullptr), _size(0) {}
        inline virtual ~Array() { free(); }

        template<typename... dimLengths>
        inline explicit Array(const dimLengths &... lens)
            : _data(nullptr), _size(0)
        {
            static_assert(sizeof...(dimLengths) == RANK,
                          "Number of arguments dismatch");
            computeLength(lens...);
            setStride();
            alloc(_length[0] * _stride[0]);
        }

        inline explicit Array(const sizeArray &len) : _data(nullptr), _size(0)
        {
            _length = len;
            setStride();
            alloc(_length[0] * _stride[0]);
        }

        // copy constructor
        inline Array(const Array &other) : _data(nullptr), _size(0)
        {
            alloc(other.size());
            copy(other);
        }

        //  assignmemt / copy assignment
        inline Array &operator=(const Array &rhs)
        {
            if (this != &rhs)
            {
                if (_size != rhs.size())
                {
                    free();
                    alloc(rhs.size());
                }
                fastCopy(rhs);
            }
            return *this;
        }

        template<typename U>
        inline typename std::enable_if_t<is_scalar<U>::value, Array &>
            operator=(const U &rhs)
        {
            const T a = static_cast<T>(rhs);
            std::fill_n(_data, _size, a);
            return *this;
        }

        // move constructor
        inline Array(Array &&other) : _data(nullptr), _size(0) { move(other); }

        // move assignment
        inline Array &operator=(Array &&rhs)
        {
            if (this != &rhs)
            {
                free();
                move(rhs);
            }
            return *this;
        }

        inline Array &operator+=(const Array &other)
        {
#pragma omp parallel for
            for (Integer i = 0; i < _size; ++i)
            {
                _data[i] += other[i];
            }
            return *this;
        }

        inline Array &operator-=(const Array &other)
        {
#pragma omp parallel for
            for (Integer i = 0; i < _size; ++i)
            {
                _data[i] -= other[i];
            }
            return *this;
        }

        inline Array &operator*=(const Array &other)
        {
#pragma omp parallel for
            for (Integer i = 0; i < _size; ++i)
            {
                _data[i] *= other[i];
            }
            return *this;
        }

        inline Array &operator/=(const Array &other)
        {
#pragma omp parallel for
            for (Integer i = 0; i < _size; ++i)
            {
                _data[i] /= other[i];
            }
            return *this;
        }

        template<typename Scalar>
        inline typename std::enable_if_t<is_scalar<Scalar>::value, Array &>
            operator*=(const Scalar &a)
        {
#pragma omp parallel for
            for (Integer i = 0; i < _size; ++i)
            {
                _data[i] *= a;
            }
            return *this;
        }

        /**
        template<typename SubType,
                 typename = typename std::enable_if<RANK == 1>::type>
        inline explicit Array(const VectorExpression<SubType> &expr) : Array()
        {
            const SubType &rhs(expr);

            _length = {rhs.size()};
            setStride();
            alloc(_length[0] * _stride[0]);

#pragma omp parallel for
            for (Integer i = 0; i < _size; ++i)
            {
                _data[i] = rhs[i];
            }
        }
        */

        template<typename SubType>
        inline Array &operator=(const VectorExpression<SubType> &expr)
        {
            const SubType &rhs(expr);
#pragma omp parallel for
            for (Integer i = 0; i < _size; ++i)
            {
                _data[i] = rhs[i];
            }
            return *this;
        }

        template<typename SubType>
        inline Array &operator+=(const VectorExpression<SubType> &expr)
        {
            const SubType &rhs(expr);
#pragma omp parallel for
            for (Integer i = 0; i < _size; ++i)
            {
                _data[i] += rhs[i];
            }
            return *this;
        }

        template<typename SubType>
        inline Array &operator-=(const VectorExpression<SubType> &expr)
        {
            const SubType &rhs(expr);
#pragma omp parallel for
            for (Integer i = 0; i < _size; ++i)
            {
                _data[i] -= rhs[i];
            }
            return *this;
        }

        template<typename SubType>
        inline Array &operator*=(const VectorExpression<SubType> &expr)
        {
            const SubType &rhs(expr);
#pragma omp parallel for
            for (Integer i = 0; i < _size; ++i)
            {
                _data[i] *= rhs[i];
            }
            return *this;
        }

        template<typename SubType>
        inline Array &operator/=(const VectorExpression<SubType> &expr)
        {
            const SubType &rhs(expr);
#pragma omp parallel for
            for (Integer i = 0; i < _size; ++i)
            {
                _data[i] /= rhs[i];
            }
            return *this;
        }

        /***********************************************************************
         * 去掉常规的实现数组按元素的四则运算，因为会产生临时变量，降低效率
         * 改用Expression Template机制实现

        Array operator+(const Array &rhs)
        {
            Array<T, RANK> result(length());
#pragma omp parallel for
            for (Integer i = 0; i < _size; ++i)
            {
                result[i] = _data[i] + rhs[i];
            }
            return result;
        }

        Array operator-(const Array &rhs)
        {
            Array<T, RANK> result(length());
#pragma omp parallel for
            for (Integer i = 0; i < _size; ++i)
            {
                result[i] = _data[i] - rhs[i];
            }
            return result;
        }

        Array operator*(const Array &rhs)
        {
            Array<T, RANK> result(length());
#pragma omp parallel for
            for (Integer i = 0; i < _size; ++i)
            {
                result[i] = _data[i] * rhs[i];
            }
            return result;
        }

        Array operator/(const Array &rhs)
        {
            Array<T, RANK> result(length());
#pragma omp parallel for
            for (Integer i = 0; i < _size; ++i)
            {
                result[i] = _data[i] / rhs[i];
            }
            return result;
        }

        template<typename Scalar>
        inline friend typename std::enable_if<is_scalar<Scalar>::value,
                                              Array<T, RANK>>::type
            operator*(const Array &lhs, const Scalar &rhs)
        {
            Array<T, RANK> result(lhs.length());
            Integer n = lhs.size();
#pragma omp parallel for
            for (Integer i = 0; i < n; ++i)
            {
                result[i] = lhs[i] * rhs;
            }
            return result;
        }

        template<typename Scalar>
        inline friend typename std::enable_if<is_scalar<Scalar>::value,
                                              Array<T, RANK>>::type
            operator*(const Scalar &lhs, const Array &rhs)
        {
            Array<T, RANK> result(rhs.length());
            Integer n = rhs.size();
#pragma omp parallel for
            for (Integer i = 0; i < n; ++i)
            {
                result[i] = rhs[i] * lhs;
            }
            return result;
        }
        ***********************************************************************/

        inline static constexpr Integer rank() noexcept { return RANK; }

        inline Integer size() const noexcept { return _size; }
        inline bool isEmpty() const noexcept { return !(_size > 0); }

        inline sizeArray length() const noexcept { return _length; }
        inline void length(const sizeArray &len) noexcept { _length = len; }
        inline Integer length(const Integer &dim) const
        {
            return _length[dim - 1];
        }

        inline T *rawData() noexcept { return _data; }
        inline const T *rawData() const noexcept { return _data; }

        inline void nullify() noexcept
        {
            _data = nullptr;
            _size = 0;
        }

        T sum(Integer istart = 0, Integer iend = -1) const
        {
            T result = static_cast<T>(0.0);
            if (_size > 0)
            {
                Integer i1 = istart;
                Integer i2 = iend >= 0 ? iend : _size - 1;
                for (Integer i = i1; i <= i2; ++i)
                {
                    result += _data[i];
                }
            }
            return result;
        }

        inline T &operator[](const Integer &i) { return _data[i]; }
        inline const T &operator[](const Integer &i) const { return _data[i]; }

        template<typename... Indices>
        inline T &operator()(const Indices &... idx) noexcept
        {
            static_assert(sizeof...(Indices) == RANK,
                          "Number of arguments dismatch");
            return _data[rawIndex(idx...)];
        }

        template<typename... Indices>
        inline const T &operator()(const Indices &... idx) const noexcept
        {
            static_assert(sizeof...(Indices) == RANK,
                          "Number of arguments dismatch");
            return _data[rawIndex(idx...)];
        }

        // 对数组所有元素按照函数表达，计算生成一个新的数组
        Array evaluate(std::function<T(const T &)> func)
        {
            Array result(_length);
            if (!isEmpty())
            {
                for (Integer i = 0; i < _size; ++i)
                {
                    result[i] = func(_data[i]);
                }
            }
            return result;
        }

        // 将函数应用到数组的每一个元素上，改变原来数组的元素
        Array &apply(std::function<void(T &)> func)
        {
            if (!isEmpty())
            {
                for (Integer i = 0; i < _size; ++i)
                {
                    func(_data[i]);
                }
            }
            return *this;
        }

        template<typename U = T>
        typename std::enable_if_t<RANK == 2, Array<U, 1>> row(
            const Integer &irow) const noexcept
        {
            Array<T, 1> vec(length(2));
            for (Integer j = 0; j < length(2); ++j)
            {
                vec(j) = (*this)(irow, j);
            }
            return vec;
        }

        inline Integer setRow(const Integer &irow,
                              const Array<T, 1> &rhs) noexcept
        {
            static_assert(RANK == 2, "Only for 2-D array");
            for (Integer j = 0; j < length(2); ++j)
            {
                (*this)(irow, j) = rhs(j);
            }
            return rhs.size();
        }

        template<typename U = T>
        typename std::enable_if_t<RANK == 2, Array<U, 1>> col(
            const Integer &jcol) const noexcept
        {
            Array<T, 1> vec(length(1));
            for (Integer i = 0; i < length(1); ++i)
            {
                vec(i) = (*this)(i, jcol);
            }
            return vec;
        }

        inline Integer setCol(const Integer &jcol,
                              const Array<T, 1> &rhs) noexcept
        {
            static_assert(RANK == 2, "Only for 2-D array");
            for (Integer i = 0; i < length(1); ++i)
            {
                (*this)(i, jcol) = rhs(i);
            }
            return rhs.size();
        }

        template<typename U = T>
        typename std::enable_if_t<RANK == 3, Array<U, 2>> row(
            const Integer &irow) const noexcept
        {
            Array<T, 2> mat(length(2), length(3));
            for (Integer j = 0; j < length(2); ++j)
            {
                for (Integer k = 0; k < length(3); ++k)
                {
                    mat(j, k) = (*this)(irow, j, k);
                }
            }
            return mat;
        }

        inline Integer setRow(const Integer &irow,
                              const Array<T, 2> &rhs) noexcept
        {
            static_assert(RANK == 3, "Only for 3-D array");
            for (Integer j = 0; j < length(2); ++j)
            {
                for (Integer k = 0; k < length(3); ++k)
                {
                    (*this)(irow, j, k) = rhs(j, k);
                }
            }
            return rhs.size();
        }

        template<typename U = T>
        typename std::enable_if_t<RANK == 3, Array<U, 2>> col(
            const Integer &jcol) const noexcept
        {
            Array<T, 2> mat(length(1), length(3));
            for (Integer i = 0; i < length(1); ++i)
            {
                for (Integer k = 0; k < length(3); ++k)
                {
                    mat(i, k) = (*this)(i, jcol, k);
                }
            }
            return mat;
        }

        inline Integer setCol(const Integer &jcol,
                              const Array<T, 2> &rhs) noexcept
        {
            static_assert(RANK == 3, "Only for 3-D array");
            for (Integer i = 0; i < length(1); ++i)
            {
                for (Integer k = 0; k < length(3); ++k)
                {
                    (*this)(i, jcol, k) = rhs(i, k);
                }
            }
            return rhs.size();
        }

        template<typename U = T>
        typename std::enable_if_t<RANK == 3, Array<U, 2>> slice(
            const Integer &kslice) const noexcept
        {
            Array<T, 2> mat(length(1), length(2));
            for (Integer i = 0; i < length(1); ++i)
            {
                for (Integer j = 0; j < length(2); ++j)
                {
                    mat(i, j) = (*this)(i, j, kslice);
                }
            }
            return mat;
        }

        inline Integer setSlice(const Integer &kslice,
                                const Array<T, 2> &rhs) noexcept
        {
            static_assert(RANK == 3, "Only for 3-D array");
            for (Integer i = 0; i < length(1); ++i)
            {
                for (Integer j = 0; j < length(2); ++j)
                {
                    (*this)(i, j, kslice) = rhs(i, j);
                }
            }
            return rhs.size();
        }

        std::ostream &print(std::ostream &os,
                            const std::vector<std::string> &headers) const
        {
            static_assert(RANK == 3, "Only for 3-D array");

            std::iostream::fmtflags oldflags(os.flags());

            os.setf(std::iostream::right | std::iostream::scientific
                    | std::iostream::uppercase);

            if (!isEmpty())
            {
                for (Integer i = 0; i < RANK; ++i)
                {
                    os << "  " << _length[i];
                }
                os << std::endl;

                constexpr int width = 16, digit = 7;
                for (Integer i = 0; i < length(1); ++i)
                {
                    os << headers[i] << std::endl;
                    for (Integer j = 0; j < length(2); ++j)
                    {
                        for (Integer k = 0; k < length(3); ++k)
                        {
                            os << std::setw(width) << std::showpoint
                               << std::setprecision(digit) << (*this)(i, j, k);
                        }
                        os << std::endl;
                    }
                }
            }

            os.flags(oldflags);
            return os;
        }

        friend std::istream &operator>>(std::istream &is, Array &obj)
        {
            Integer sz = 1, len[RANK];
            for (Integer i = 0; i < RANK; ++i)
            {
                is >> len[i];
                obj._length[i] = len[i];
                sz *= len[i];
            }
            is >> std::ws;

            if (sz > 0)
            {
                obj.free();
                obj.alloc(sz);
                obj.setStride();

                for (Integer i = 0; i < sz; ++i)
                {
                    is >> obj[i];
                }
                is >> std::ws;
            }
            else
            {
                obj = Array();
            }

            return is;
        }

        friend std::ostream &operator<<(std::ostream &os, const Array &obj)
        {
            if (!obj.isEmpty())
            {
                std::iostream::fmtflags oldflags(os.flags());

                os.setf(std::iostream::right | std::iostream::scientific
                        | std::iostream::uppercase);

                for (Integer i = 0; i < RANK; ++i)
                {
                    os << "  " << obj._length[i];
                }
                os << std::endl;

                constexpr int width = 16, digit = 7;

                for (Integer i = 0; i < obj.size(); ++i)
                {
                    os << std::setw(width) << std::showpoint
                       << std::setprecision(digit) << obj[i];
                    if ((i + 1) % obj.length(RANK) == 0) os << std::endl;
                }

                os.flags(oldflags);
            }
            else
            {
                for (Integer i = 0; i < RANK; ++i)
                {
                    os << "  " << 0;
                }
                os << std::endl;
            }

            return os;
        }
    };


    template<typename U>
    using Vector = Array<U, 1>;

}  // namespace cppkit
